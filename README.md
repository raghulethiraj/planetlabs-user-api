Author : Raghul Ethiraj (raghul@iastate.edu)
Description: A RESTful API that manages user records and user groups
Programming Language: Python
Framework used: Flask

Project description:
This implementation of REST API ignores the persistence layer but rather uses arrays to manage data management. 

All POST and PUT requests are expected to be a valid JSON object. 
Note - ["aperez","jsmith"] is considered a valid JSON list in this implementation

Setup information:

This project was set up using a virtualenv for flask. Simply switch to the source directory and run
".\flask\Scripts\pip install flask"

Note - You might also need pip to install the flask framework

After this, run the userapi.py using "python userapi.py"

This project also comes with a set of unit testing cases. To execute the test cases, run "python apptests.py".


#!flask/bin/python
from flask import Flask, jsonify, abort, make_response, request

app = Flask(__name__)

#A list of users. Usually populated from a database
users = [
    {
        "first_name": "Joe",
        "last_name": "Smith",
        "userid": "jsmith",
        "groups": ["admins", "users"]
    },
    {
        "first_name": "Alex",
        "last_name": "Perez",
        "userid": "aperez",
        "groups": ["users"]
    },
    {
        "first_name": "Ana",
        "last_name": "Collazo",
        "userid": "acollazo",
        "groups": ["admins", "superheros", "users"]
    },
    {
        "first_name": "Brandon",
        "last_name": "McDonald",
        "userid": "bmcdonald",
        "groups": ["superheros", "users"]
    }
]

#All the user groups a user could be in
user_groups = ["admins", "superheros","users"]

#Return user information for user_id in a JSON format or 404 if none exist
@app.route('/api/v1.0/users/<user_id>', methods=['GET'])
def get_user(user_id):
    #Query for the user and return them in JSON format if found
    user = [user for user in users if user['userid'] == user_id]

    #If no user found, abort with 404
    if len(user) == 0:
        abort(404)
    return jsonify(user[0])

#Creates a new user with valid user record
@app.route('/api/v1.0/users', methods=['POST'])
def create_user():
    #Abort request if user record is not valid
    if not request.json or not 'first_name' in request.json or not 'last_name' in request.json or not 'userid' in request.json:
        abort(400)
    
    #Querying to see if the POST's user already exists
    existingUser = [user for user in users if user['userid'] == request.json['userid']]

    #If POST is an existing user, abort with 403 forbidden
    if len(existingUser) > 0:
        abort(403)

    #If first_name and last_name contains alphanumeric character, abort with 400
    if not request.json['first_name'].isalpha() or not request.json['last_name'].isalpha():
        abort(400)

    #Create a user with the POST values
    user = {
        'first_name': request.json['first_name'],
        'last_name': request.json['last_name'],
        'userid': request.json['userid'],
        'groups': request.json.get('groups', []) #A user doesn't have to be assigned to a group when created
    }

    #Add the user to the users array
    users.append(user)

    #Return the inserted user with a 201 
    return make_response(jsonify(user), 201)

#Deletes the user with user_id, returns 404 if none exist
@app.route('/api/v1.0/users/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    #Check to see if the user exists, else abort with 404
    user = [user for user in users if user['userid'] == user_id]
    if len(user) == 0:
        abort(404)

    #Remove user and return true
    users.remove(user[0])
    return make_response(jsonify({'result': True}), 200)

#Updates the user with user_id with valid user record, returns 404 if none exist
@app.route('/api/v1.0/users/<user_id>', methods=['PUT'])
def update_user(user_id):
    #If not JSON, abort with 400
    if not request.json:
        abort(400)

    #Check to see if the user exists, else abort with 404
    user = [user for user in users if user['userid'] == user_id]
    if len(user) == 0:
        abort(404)

    #If first_name and last_name contains alphanumeric character, abort with 400
    if 'first_name' in request.json and not request.json['first_name'].isalpha():
        abort(400)

    if 'last_name' in request.json and not request.json['last_name'].isalpha():
        abort(400)

    #Update the user record by defaulting with previous values
    user[0]['first_name']   = request.json.get('first_name', user[0]['first_name'])
    user[0]['last_name']    = request.json.get('last_name', user[0]['last_name'])
    user[0]['userid']       = request.json.get('userid', user[0]['userid'])

    #Only add non empty group array values
    groups                  = request.json.get('groups', user[0]['groups'])
    groups                  = [group for group in groups if group != ""]
    user[0]['groups']       = groups

    #Return the updated user record
    return make_response(jsonify(user[0]),200)

#Returns a JSON list of user_ids in group_name
@app.route('/api/v1.0/groups/<group_name>', methods=['GET'])
def get_groupusers(group_name):
    #Check to see if the group exists
    group = [group for group in user_groups if group == group_name]

    #Abort with 404 if no group found with given group_name
    if len(group) == 0:
        abort(404)

    #Empty list of users in a group
    group_users = []

    #Check every user to see if they are in the given group
    for user in users:
        in_group = [group for group in user['groups'] if group == group_name]

        #Add this user to the list if they are in the group
        if len(in_group) > 0 :
            group_users.append(user['userid'])

    return make_response(jsonify({"users":group_users}),200)
    

#Creates an empty group with the "name" given in the req body
@app.route('/api/v1.0/groups', methods=['POST'])
def create_group():
    #Abort request if request is not valid
    if not request.json or not 'name' in request.json:
        abort(400)
    
    #Querying to see if the POST's group already exists
    existingGroup = [group for group in user_groups if group == request.json['name']]

    #If POST is an existing group, abort
    if len(existingGroup) > 0:
        abort(403)

    #If empty group name, abort
    name = request.json['name'].replace(" ","")
    if len(name) == 0:
        abort(400)

    #Create a group with the POST 'name'
    user_groups.append(name)

    #Return the inserted group with a 201 
    return make_response(jsonify({"group":user_groups[-1]}), 201)

#Updates the group_name with members in the req body
#Takes input in ["jsmith","aperez"] format
@app.route('/api/v1.0/groups/<group_name>', methods=['PUT'])
def update_group(group_name):
    #if request is not a valid JSON, abort
    if not request.json:
        abort(400)

    #Querying to see if the PUT's group already exists
    existingGroup = [group for group in user_groups if group == group_name]

    #If group doesn't exist, abort
    if len(existingGroup) == 0:
        abort(400)

    #Remove all users who are already in the group_name
    for user in users:
        if group_name in user['groups']:
            user['groups'].remove(group_name)

    #Add users to the group as given in the PUT body
    for user_id in request.json:
        user = [user for user in users if user['userid'] == user_id]
        if len(user) > 0:
            user[0]['groups'].append(group_name)

    #Preparing the updated membership list for the group_name

    #Empty list of users in a group
    group_users = []

    #Check every user to see if they are in the given group
    for user in users:
        in_group = [group for group in user['groups'] if group == group_name]

        #Add this user to the list if they are in the group
        if len(in_group) > 0 :
            group_users.append(user['userid'])

    return make_response(jsonify({"users":group_users}),200)


#Deletes a group
@app.route('/api/v1.0/groups/<group_name>', methods=['DELETE'])
def delete_group(group_name):
    #Querying to see if the PUT's group already exists
    existingGroup = [group for group in user_groups if group == group_name]

    #If group doesn't exist, abort
    if len(existingGroup) == 0:
        abort(400)

    #Remove all users who are already in the group_name
    for user in users:
        if group_name in user['groups']:
            user['groups'].remove(group_name)

    #Remove the group from the user_groups array
    user_groups.remove(group_name)

    return make_response(jsonify({'result': True}), 200)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(403)
def not_found(error):
    return make_response(jsonify({'error': 'Action not allowed'}), 403)

@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)

if __name__ == '__main__':
    app.run(debug=True)
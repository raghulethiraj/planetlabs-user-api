import userapi
import unittest
import json

class TestAppAPI(unittest.TestCase):

  def setUp(self):
  		userapi.app.config['TESTING'] = True
  		userapi.user_groups = ["admins","superheros","users"]
  		userapi.users = [
		    {
		        "first_name": "Joe",
		        "last_name": "Smith",
		        "userid": "jsmith",
		        "groups": ["admins", "users"]
		    },
		    {
		        "first_name": "Alex",
		        "last_name": "Perez",
		        "userid": "aperez",
		        "groups": ["users"]
		    },
		    {
		        "first_name": "Ana",
		        "last_name": "Collazo",
		        "userid": "acollazo",
		        "groups": ["admins", "superheros", "users"]
		    },
		    {
		        "first_name": "Brandon",
		        "last_name": "McDonald",
		        "userid": "bmcdonald",
		        "groups": ["superheros", "users"]
		    }
		]
		
  		self.app = userapi.app.test_client()

  #Test to verify the retrived user info
  def test_get_user(self):
  		resp_obj = self.app.get('/api/v1.0/users/aperez')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		self.assertEqual('Alex', resp['first_name'])

  		#GETing a non exsistant user
  		resp_obj = self.app.get('/api/v1.0/users/hpotter')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('Not found', resp['error'])

  #Test to POST a new user, retrive it and check for correct user data and POST criteria
  def test_post_user(self):
  		#POSTing a new user
  		self.app.post('/api/v1.0/users', data=json.dumps(dict(
  			first_name = "Raghul",
  			last_name = "Ethiraj",
  			userid = "rethiraj",
  			groups = ["admins"]
  		)), content_type='application/json')

  		#Retriving it
  		resp_obj = self.app.get('/api/v1.0/users/rethiraj')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test successful retrive
  		self.assertEqual('Raghul', resp['first_name'])

  		#POSTing a bad user data
  		resp_obj = self.app.post('/api/v1.0/users', data=json.dumps(dict(
  			first_name = "Apl4ha",
  			last_name = "B3t4",
  			userid = "ab3t4",
  			groups = ["admins"]
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test for bad request
  		self.assertEqual('Bad request', resp['error'])

  		#Testing for POSTing to exsisting user
  		resp_obj = self.app.post('/api/v1.0/users', data=json.dumps(dict(
  			first_name = "Raghul",
  			last_name = "Ethiraj",
  			userid = "rethiraj",
  			groups = ["admins"]
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('Action not allowed', resp['error'])

  #Test to DELETE a user and check for deletion of non exisitant users
  def test_delete_user(self):
  		#Deleting an existing user
  		resp_obj = self.app.delete('/api/v1.0/users/jsmith')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Check successful deletion
  		self.assertTrue(resp['result'])

  		#Retriving deleted user
  		resp_obj = self.app.get('/api/v1.0/users/jsmith')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('Not found', resp['error'])

  #Test to put to an exsisting user and check for errors with bad user data
  def test_put_user(self):
  		#Testing for PUTing to an exsisting user
  		resp_obj = self.app.put('/api/v1.0/users/jsmith', data=json.dumps(dict(
  			last_name = "Bourne"
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Check if last_name was updated correctly
  		self.assertEqual('Bourne', resp['last_name'])

  		#Testing for PUTing a bad user record
  		resp_obj = self.app.put('/api/v1.0/users/jsmith', data=json.dumps(dict(
  			last_name = "B0urn3"
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test for bad request
  		self.assertEqual('Bad request', resp['error'])

  #Test to PUT a non exsistant user record
  def test_put_user_doe(self):
  		#Testing for PUTing to an non exsistant user
  		resp_obj = self.app.put('/api/v1.0/users/hpotter', data=json.dumps(dict(
  			last_name = "Potter"
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('Not found', resp['error'])

  #Test for user groups and check for errors with GETing non existant group
  def test_admins(self):
  		resp_obj = self.app.get('/api/v1.0/groups/admins')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		assert 'jsmith','acollazo' in resp['users']

  		resp_obj = self.app.get('/api/v1.0/groups/heros')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('Not found', resp['error'])

  #Test to POST a new group name. Check for errors w/ POSTing to exsisting group name or no 'name' param
  def test_post_groupname(self):
  		#Testing for POSTing a new group name
  		resp_obj = self.app.post('/api/v1.0/groups', data=json.dumps(dict(
  			name = "custodians"
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('custodians', resp['group'])

  		#Testing for POSTing a exisiting group name
  		resp_obj = self.app.post('/api/v1.0/groups', data=json.dumps(dict(
  			name = "custodians"
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('Action not allowed', resp['error'])

  		#Testing for POSTing w/o a 'name' param
  		resp_obj = self.app.post('/api/v1.0/groups', data=json.dumps(dict(
  			group_name = "custodians"
  		)), content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test error
  		self.assertEqual('Bad request', resp['error'])

  #Test to update a group's member list
  def test_put_groupmembers(self):
  		#Testing for PUTing a user list to group
  		resp_obj = self.app.put('/api/v1.0/groups/admins', data=json.dumps(["aperez"]),content_type='application/json')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Check to see if 'aperez' is registered as an admin
  		self.assertTrue('aperez' in resp['users'])

  		#Check to see if other users are no longer admins
  		self.assertFalse('jsmith' in resp['users'])

  #Test to DELETE a group_name and check to see if users are not associated with the group_name after deletion
  def test_delete_group(self):
		#Testing for DELETEing a group
  		resp_obj = self.app.delete('/api/v1.0/groups/admins')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Check to see if group was deleted successfully
  		self.assertTrue(resp['result'])

  		#Retrive a user record who was an admin and see if they are still associated with the deleted group_name
  		resp_obj = self.app.get('/api/v1.0/users/jsmith')
  		resp = json.loads(resp_obj.data.decode(encoding='UTF-8'))

  		#Test to see if this user still has 'admin' in their 'groups'
  		self.assertFalse('admin' in resp['groups'])

if __name__ == '__main__':
    unittest.main()
